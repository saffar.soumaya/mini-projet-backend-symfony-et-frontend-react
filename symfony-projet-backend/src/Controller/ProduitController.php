<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Produit;
/**
 * @Route("/api", name="api_")
 */
class ProduitController extends AbstractController
{
    /**
     * @Route("/produit", name="produit", methods={"GET"})
     */
    public function index(): Response
    {
        $produits = $this->getDoctrine()
            ->getRepository(Produit::class)
            ->findAll();
  
        $data = [];
  
        foreach ($produits as $produit) {
           $data[] = [
               'id' => $produit->getId(),
               'nom' => $produit->getNom(),
               'prix' => $produit->getPrix(),
           ];
        }
  
  
        return $this->json($data);
    }
     /**
     * @Route("/produit/new", name="produit_new", methods={"post"})
     * 
     */
    public function new(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
  
        $produit = new Produit();
        $produit->setNom($request->request->get("nom"));
        $produit->setPrix($request->request->get("prix"));
  
        $entityManager->persist($produit);
        $entityManager->flush();
  
        return $this->json('Created new produit  successfully with id ' . $produit->getId());
    }
    /**
     * @Route("/show/{id}", name="produit_show", methods={"GET"})
     */
    public function show(int $id): Response
    {
        $produit = $this->getDoctrine()
            ->getRepository(produit::class)
            ->find($id);
  
        if (!$produit) {
  
            return $this->json('No produit found for id' . $id, 404);
        }
  
        $data =  [
            'id' => $produit->getId(),
            'nom' => $produit->getNom(),
            'prix' => $produit->getPrix(),
        ];
          
        return $this->json($data);
    }
    /**
     * @Route("/produit/edit/{id}", name="produit_edit", methods={"put"})
     */
    public function edit(Request $request, int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $produit = $entityManager->getRepository(Produit::class)->find($id);
  
        if (!$produit) {
            return $this->json('No produit found for id' . $id, 404);
        }
         
        $content = json_decode($request->getContent());
         
        $produit->setNom($content->nom);
        $produit->setPrix($content->prix);
        $entityManager->flush();
  
        $data =  [
            'id' => $produit->getId(),
            'nom' => $produit->getNom(),
            'prix' => $produit->getPrix(),
        ];
          
        return $this->json($data);
    }
    /**
     * @Route("/produit/{id}", name="produit_delete", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $produit = $entityManager->getRepository(Produit::class)->find($id);
  
        if (!$produit) {
            return $this->json('No produit found for id' . $id, 404);
        }
  
        $entityManager->remove($produit);
        $entityManager->flush();
  
        return $this->json('Deleted a produit successfully with id ' . $id);
    }
  
}
