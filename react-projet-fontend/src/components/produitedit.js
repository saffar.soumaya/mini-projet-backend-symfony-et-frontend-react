import React, { useState, useEffect } from 'react';
import { Link, useParams } from "react-router-dom";
import Layout from "../components/Layout"

import Swal from 'sweetalert2'
import axios from 'axios';
  
function ProduitEdit() {
    const [id, setId] = useState(useParams().id)
    const [nom, setNom] = useState('');
    const [prix, setPrix] = useState('')
    const [isSaving, setIsSaving] = useState(false)
  
      
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/show/${id}`)
        .then(function (response) {
            let produit = response.data
            setNom(produit.nom);
            setPrix(produit.prix);
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'An Error Occured!',
                showConfirmButton: false,
                timer: 1500
            })
        })
          
    }, [])
  
  
    const handleSave = () => {
        setIsSaving(true);
        axios.put(`http://127.0.0.1:8000/api/produit/edit/${id}`, {
            nom: nom,
            prix: prix
        })
        .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: 'produit updated successfully!',
                showConfirmButton: false,
                timer: 1500
            })
            setIsSaving(false);
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'Erreur',
                title: 'Une erreur sest produite',
                showConfirmButton: false,
                timer: 1500
            })
            setIsSaving(false)
        });
    }
  
  
    return (
        <Layout>
            <div className="container">
                <h2 className="text-center mt-5 mb-3"> Modifier produit</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/produit">liste de produit
                        </Link>
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Nom</label>
                                <input 
                                    onChange={(event)=>{setNom(event.target.value)}}
                                    value={nom}
                                    type="text"
                                    className="form-control"
                                    id="nom"
                                    name="nom"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="name">Prix</label>
                                <input 
                                    value={prix}
                                    onChange={(event)=>{setPrix(event.target.value)}}
                                    className="form-control"
                                    id="prix"
                                    name="prix"></input>
                            </div>
                            <button 
                                disabled={isSaving}
                                onClick={handleSave} 
                                type="button"
                                className="btn btn-outline-success mt-3">
                                modifier produit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default ProduitEdit;