import React,{ useState, useEffect} from 'react';
import { Link } from "react-router-dom";
import Layout from "../components/Layout"
import Swal from 'sweetalert2'
import axios from 'axios';

 
function ProduitList() {
    const  [produitList, setProduitList] = useState([])
  
    useEffect(() => {
        fetchProduitList()
     }, [])
  
     const fetchProduitList = () => {
   
     }
  
    useEffect(()=>{

        axios.get('http://127.0.0.1:8000/api/produit')
        .then(function (response) {
            console.log("####",response)
          setProduitList(response.data);
        })
        .catch(function (Erreur) {
          console.log(Erreur);
        })

    },[])

    /////////////////
    const handleDelete = (id) => {
        Swal.fire({
            title: 'Es-tu sûr?',
            text: "Vous ne pourrez pas revenir en arrière !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui, supprimez-le !'
          }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`http://127.0.0.1:8000/api/produit/${id}`)
                .then(function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Produit supprimé avec succès',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    fetchProduitList()
                })
                .catch(function (Error) {
                    Swal.fire({
                        icon: 'Erreur',
                        title: 'Une erreur sest produite!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                });
            }
          })
    }
  
    return (
        <Layout>
           <div className="container">
            <h2 className="text-center mt-5 mb-3">Liste des produits</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-primary"
                            to="/create">Ajoute nouvelle produit
                        </Link>
                    </div>
                    <div className="card-body">
              
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="90px">Nom</th>
                                    <th width="90px">Prix</th>
                                    <th width="90px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {produitList.map((produit, key)=>{
                                    return (
                                        <tr key={key}>
                                            <td>{produit.nom}</td>
                                            <td>{produit.prix}</td>
                                            <td>
                                                <Link
                                                    to={`/show/${produit.id}`}
                                                    className="btn btn-outline-info">
                                                    Détails
                                                </Link>
                                                <Link
                                                    className="btn btn-outline-success mx-1"
                                                    to={`/edit/${produit.id}`}>
                                                    Modifier
                                                </Link>
                                                <button 
                                                    onClick={()=>handleDelete(produit.id)}
                                                    className="btn btn-outline-danger mx-1">
                                                    Supprimer
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default ProduitList;