import React, {useState, useEffect} from 'react';
import { Link, useParams } from "react-router-dom";
import axios from 'axios';
import Layout from "../components/Layout"
 
function ProduitShow() {
    const [id, setId] = useState(useParams().id)
    const [produit, setProduit] = useState({nom:'', prix:''})
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/show/${id}`)
        .then(function (response) {
          setProduit(response.data)
        })
        .catch(function (error) {
          console.log(error);
        })
    }, [])
  
    return (
        <Layout>
           <div className="container">
            <h2 className="text-center mt-5 mb-3">Détails Produit</h2>
                <div className="card">
                    <div className="card-header">
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/produit"> Liste des produits
                        </Link>
                    </div>
                    <div className="card-body">
                        <b className="text-muted">Nom:</b>
                        <p>{produit.nom}</p>
                        <b className="text-muted">Prix:</b>
                        <p>{produit.prix}</p>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
  
export default ProduitShow;