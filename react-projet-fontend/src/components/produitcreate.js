import React, {useState} from 'react';
import { Link } from "react-router-dom";
import Layout from "../components/Layout"

import Swal from 'sweetalert2'
import axios from 'axios';
  
function ProduitCreate() {
    const [nom, setNom] = useState('');
    const [prix, setPrix] = useState('')
    const [isSaving, setIsSaving] = useState(false)
  
    const handleSave = () => {
        setIsSaving(true);
        let formData = new FormData()
        formData.append("nom", nom)
        formData.append("prix", prix)


        axios.post('http://127.0.0.1:8000/api/produit/new',formData)
          .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: 'Produit ajouter avec succès!',
                showConfirmButton: false,
                timer: 1500
            })
            setIsSaving(false);
            setNom('')
            setPrix('')
          })
          .catch(function (error) {
            Swal.fire({
                icon: 'erreur',
                title: 'Une erreur sest produite!',
                showConfirmButton: false,
                timer: 1500
            })
            setIsSaving(false)
          });
    }
  
    return (
        <Layout>
            <div className="container">
                <h2 className="text-center mt-5 mb-3">Ajouter nouvelle produit</h2>
                <div className="card">
                    <div className="card-header" widh="140px " >
                        <Link 
                            className="btn btn-outline-info float-right"
                            to="/produit">liste de produit
                        </Link>
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Nom</label>
                                <input 
                                    onChange={(event)=>{setNom(event.target.value)}}
                                    value={nom}
                                    type="text"
                                    className="form-control"
                                    id="nom"
                                    name="nom"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="name">Prix</label>
                                <input 
                                 onChange={(event)=>{setPrix(event.target.value)}}
                                    value={prix}
                                    type="text"
                                    className="form-control"
                                    id="prix"
                                   
                                    name="prix"></input>
                            </div>
                            <button 
                                disabled={isSaving}
                                onClick={handleSave} 
                                type="button"
                                className="btn btn-outline-primary mt-3">
                                ajouter Produit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
export default ProduitCreate;