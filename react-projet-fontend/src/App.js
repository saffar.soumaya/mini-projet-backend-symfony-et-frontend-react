import React, { Component } from "react";
import "./index.css";
import { BrowserRouter as Router, Switch, Route, withRouter } from 'react-router-dom';




import ProduitList from "./components/produitliste"
import ProduitCreate from "./components/produitcreate"
import ProduitEdit from "./components/produitedit"
import ProduitShow from "./components/produitshow"


class App extends Component {


  render() {
    return (

      <Router>
    
        <div>


          <Switch>
            
            {/**login */}
            
            <Route path="/produit"  component={ProduitList} />
            <Route path="/create"  component={ProduitCreate} />
            <Route path="/edit/:id"  component={ProduitEdit} />
            <Route path="/show/:id"  component={ProduitShow} />
           

          </Switch>
        </div>
      </Router>

    );
  }
}

export default withRouter(App);
