import React from "react";
import ReactDOM from 'react-dom';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./index.css";
import App from "./App";
import { BrowserRouter } from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';


// axios.defaults.baseURL = window.location.protocol + "//" + window.location.hostname + ":8000";
//window.apiUrl = "http://192.168.3.91:8000/"
window.apiUrl = "http://0bba-102-156-222-87.ngrok.io/"
//window.apiUrl = "http://390908dfec7f.ngrok.io/"
//window.apiUrl = "http://192.168.3.80:8000/"
//window.apiUrl = "http://192.168.3.80:8000/"
//window.apiUrl = "http://127.0.0.1:8000/"
//window.apiUrl = "http://192.168.3.80:8000/"
//window.apiUrl=window.location.protocol + "//" + window.location.hostname + ":8000/";
ReactDOM.render(

    <BrowserRouter>
        <App/>
    </BrowserRouter>
    , document.getElementById('root'));
registerServiceWorker();